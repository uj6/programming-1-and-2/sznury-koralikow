//Zuzanna Domagala
#include <iostream>

using namespace std;

bool pierwszy_mniejszy(char a, char b, char c, char a_1, char b_1, char c_1){
    if(a < a_1) return true;
    else if(a == a_1){
        if(b < b_1) return true;
        else if(b == b_1){
            if(c < c_1) return true;
            else return false;
        }
    }
    return false;
}

bool pierwszy_mniejszy(char a, char b, char c, int d, char a_1, char b_1, char c_1, int d_1){
    if(a < a_1) return true;
    else if(a == a_1){
        if(b < b_1) return true;
        else if(b == b_1){
            if(c < c_1) return true;
            else if(c == c_1){
                if(d < d_1) return true;
                else return false;
            }
        }
    }
    return false;
}

struct koralik{
    int id_kor; //identyfikator korala
    char a;
    char b;
    char c;
    int ile_powiazan;
    int ile_powiazan_do;
    struct powiazania{
        koralik* obecny;
        powiazania* nastepny;
        powiazania* poprzedni;
    };
    powiazania* pierwszy = new powiazania; //powiazanie. ktore robi ten koral
    powiazania* pierwszy_do = new powiazania; //powiazanie, ktore jest do niego

    void konstrukcja(int identyfikator, char a1, char b1, char c1){ //konstruktor korala
        pierwszy->nastepny = NULL;
        ile_powiazan = 0;
        ile_powiazan_do = 0;
        pierwszy_do->nastepny = NULL;
        id_kor = identyfikator;
        a = a1;
        b = b1;
        c = c1;
    }

    void powiazanie(koralik* do_kogo){ //koral tworzy powiazanie
        if(ile_powiazan == 0){
            pierwszy->obecny = do_kogo;
            ile_powiazan++;
        }
        else{
            powiazania* newElem = new powiazania;
            newElem->obecny = do_kogo;
            newElem->nastepny = pierwszy;
            pierwszy->poprzedni = newElem;
            pierwszy = newElem;
            pierwszy->poprzedni = NULL;
            ile_powiazan++;

        }
    }

    void powiazanie_do(koralik* od_kogo){ //inny koral tworzy powiazanie z koralem
        if(ile_powiazan_do == 0){
            pierwszy_do->obecny = od_kogo;
            ile_powiazan_do++;
        }
        else{
            powiazania* newElem = new powiazania;
            newElem->obecny = od_kogo;
            newElem->nastepny = pierwszy_do;
            pierwszy_do->poprzedni = newElem;
            pierwszy_do = newElem;
            pierwszy_do->poprzedni = NULL;
            ile_powiazan_do++;


        }
    }

    void delete_powiazanie(koralik* od_tego){
        int i = 0;
        for(powiazania* walkElem = od_tego->pierwszy;i < od_tego->ile_powiazan; walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_kor == id_kor){

                if(od_tego->ile_powiazan == 1){
                    od_tego->ile_powiazan = 0;
                }
                else{
                    if(i != 0){
                        walkElem->poprzedni->nastepny = walkElem->nastepny;
                    }
                    else{
                        walkElem->nastepny->poprzedni = NULL;
                        od_tego->pierwszy = walkElem->nastepny;
                    }
                    if(i != od_tego->ile_powiazan - 1){
                        walkElem->nastepny->poprzedni = walkElem->poprzedni;
                    }
                    else{
                        walkElem->poprzedni->nastepny = NULL;
                    }
                    od_tego->ile_powiazan--;
                    break;
                }
            }
        }
        //usuwanie powiazania do korala z tego korala
        i = 0;
        for(powiazania* walkElem = pierwszy_do;(i < ile_powiazan_do); walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_kor == od_tego->id_kor){
                if(ile_powiazan_do == 1){
                    ile_powiazan_do = 0;
                }
                else{
                    if(i != 0){
                        walkElem->poprzedni->nastepny = walkElem->nastepny;
                    }
                    else{
                        walkElem->nastepny->poprzedni = NULL;
                        pierwszy_do = walkElem->nastepny;
                    }
                    if(i != ile_powiazan_do - 1){
                        walkElem->nastepny->poprzedni = walkElem->poprzedni;
                    }
                    else{
                        walkElem->poprzedni->nastepny = NULL;
                    }
                    ile_powiazan_do--;
                    break;
                }
            }
        }
    }

    void delete_everything(){ //funkcja usuwa wszytskie powiazania idace do tego korala
        int j = 0;
        for(powiazania* walkElem = pierwszy_do; j < ile_powiazan_do; walkElem = walkElem->nastepny, j++){
            koralik* od_tego = new koralik;
            od_tego = walkElem->obecny;
            int i = 0;
            for(powiazania* walkElem = od_tego->pierwszy;i < od_tego->ile_powiazan; walkElem = walkElem->nastepny, i++){
                if(walkElem->obecny->id_kor == id_kor){
                    if(od_tego->ile_powiazan == 1){
                        od_tego->ile_powiazan = 0;
                    }
                    else{
                        if(i != 0){
                            walkElem->poprzedni->nastepny = walkElem->nastepny;
                        }
                        else{
                            walkElem->nastepny->poprzedni = NULL;
                            od_tego->pierwszy = walkElem->nastepny;
                        }
                        if(i != od_tego->ile_powiazan - 1){
                            walkElem->nastepny->poprzedni = walkElem->poprzedni;
                        }
                        else{
                            walkElem->poprzedni->nastepny = NULL;
                        }
                        od_tego->ile_powiazan--;
                        break;
                    }
                }
            }
        }
    }


    void display_powiazania(){
        powiazania* najmniejszy = pierwszy;
        char e, f, g;
        e = f = g = '1';
        int h = -1;
        for(int j = 0; j < ile_powiazan; j++){
            int i = 0;
            for(powiazania* walkElem = pierwszy;(i < ile_powiazan); walkElem = walkElem->nastepny, i++){
                if(pierwszy_mniejszy(e, f, g, h, walkElem->obecny->a, walkElem->obecny->b, walkElem->obecny->c, walkElem->obecny->id_kor)){
                    if (pierwszy_mniejszy(walkElem->obecny->a, walkElem->obecny->b, walkElem->obecny->c, walkElem->obecny->id_kor,
                                         najmniejszy->obecny->a, najmniejszy->obecny->b, najmniejszy->obecny->c, najmniejszy->obecny->id_kor) ||
                        pierwszy_mniejszy(najmniejszy->obecny->a, najmniejszy->obecny->b, najmniejszy->obecny->c, najmniejszy->obecny->id_kor, e, f, g, h)
                        || (najmniejszy->obecny->a == e && najmniejszy->obecny->b == f && najmniejszy->obecny->c == g && najmniejszy->obecny->id_kor == h)){
                            najmniejszy = walkElem;
                    }
                }
            }
            cout << " " << najmniejszy->obecny->a << najmniejszy->obecny->b << najmniejszy->obecny->c << " " << najmniejszy->obecny->id_kor;
            e = najmniejszy->obecny->a;
            f = najmniejszy->obecny->b;
            g = najmniejszy->obecny->c;
            h = najmniejszy->obecny->id_kor;
        }
    }

    void display_powiazaniado(){
        int i = 0;
        for(powiazania* walkElem = pierwszy_do;(i < ile_powiazan_do); walkElem = walkElem->nastepny, i++){
            cout << walkElem->obecny->id_kor << " ";
        }
    }
};


struct lista_sznura{ //lista korali podczepionych do sznura
    koralik* obecny;
    lista_sznura* nastepny;
    lista_sznura* poprzedni;
};

struct sznur{
    char id_1;
    char id_2;
    char id_3;
    lista_sznura* pierwszy;
    int ile_korali;
    void konstrukcja(char a, char b, char c){
        ile_korali = 0;
        id_1 = a;
        id_2 = b;
        id_3 = c;
    }

    void add_koral(int x){ //dodawanie korala do sznura
        koralik* dodawany = new koralik;
        dodawany->konstrukcja(x, id_1, id_2, id_3);
        if(ile_korali == 0){
            lista_sznura* newElem = new lista_sznura;
            newElem->obecny = dodawany;
            newElem->nastepny = NULL;
            pierwszy = newElem;
            ile_korali++;
        }
        else{
            lista_sznura* newElem = new lista_sznura;
            newElem->obecny = dodawany;
            newElem->nastepny = pierwszy;
            pierwszy->poprzedni = newElem;
            pierwszy = newElem;
            pierwszy->poprzedni = NULL;
            ile_korali++;
        }
    }

    void delete_koral(int id){
        bool nie_znalazl_sie = true;
        int i = 0;
        for(lista_sznura* walkElem = pierwszy;nie_znalazl_sie && (i < ile_korali); walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_kor == id){ //szuka elementu
                //usuwa wszytskie powiazania z tym koralem
                walkElem->obecny->delete_everything();
                //usuwanie korala z listy korali sznura
                if(ile_korali == 1){
                    ile_korali = 0;
                }
                else{
                    if(i != 0){
                        walkElem->poprzedni->nastepny = walkElem->nastepny;
                    }
                    else{
                        walkElem->nastepny->poprzedni = NULL;
                        pierwszy = walkElem->nastepny;
                    }
                    if(i != ile_korali - 1){
                        walkElem->nastepny->poprzedni = walkElem->poprzedni;
                    }
                    else{
                        walkElem->poprzedni->nastepny = NULL;
                    }
                    ile_korali--;
                }
                nie_znalazl_sie = false;
            }
        }
    }

    void przenies_koral(int id, sznur* docelowy){
        int i = 0;
        for(lista_sznura* walkElem = pierwszy; i < ile_korali; walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_kor == id){ //koral sie znalazl
                //usuniecie korala ze sznura
                if(ile_korali == 1){
                    ile_korali = 0;
                }
                else{
                    if(i != 0){
                        walkElem->poprzedni->nastepny = walkElem->nastepny;
                    }
                    else{
                        walkElem->nastepny->poprzedni = NULL;
                        pierwszy = walkElem->nastepny;
                    }
                    if(i != ile_korali - 1){
                        walkElem->nastepny->poprzedni = walkElem->poprzedni;
                    }
                    else{

                        walkElem->poprzedni->nastepny = NULL;
                    }
                    ile_korali--;
                }

                //dodanie korala do docelowego sznura
                if(docelowy->ile_korali == 0){
                    docelowy->pierwszy = walkElem;
                    docelowy->ile_korali = 1;
                }
                else{
                    lista_sznura* newElem = new lista_sznura;
                    newElem->obecny = walkElem->obecny;
                    newElem->nastepny = docelowy->pierwszy;
                    docelowy->pierwszy->poprzedni = newElem;
                    docelowy->pierwszy = newElem;
                    docelowy->pierwszy->poprzedni = NULL;
                    docelowy->ile_korali = docelowy->ile_korali + 1;
                }
                break;
            }
        }
    }

    void display(){
        cout << id_1 << id_2 << id_3 << endl; //wyswietla id sznura
        int a = -1;
        lista_sznura* najmniejszy = pierwszy;
        for(int j = 0; j < ile_korali; j++){
            int i = 0;
            for(lista_sznura* walkElem = pierwszy;i < ile_korali; walkElem = walkElem->nastepny, i++){
                if(a < walkElem->obecny->id_kor){
                    if(walkElem->obecny->id_kor < najmniejszy->obecny->id_kor ||
                       (najmniejszy->obecny->id_kor <= a)){
                        najmniejszy = walkElem;
                    }
                }
            }
            cout << najmniejszy->obecny->id_kor;
            najmniejszy->obecny->display_powiazania();
            a = najmniejszy->obecny->id_kor;
            cout << endl;
        }
    }

    koralik* find_koral(int id){
        int i = 0;
        for(lista_sznura* walkElem = pierwszy; i < ile_korali; walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_kor == id){
                return walkElem->obecny;
            }
        }
        return NULL;
    }
};

struct sznur_lista{
    sznur* obecny;
    sznur_lista* nastepny;
    sznur_lista* poprzedni;
};

struct konstrukcja{
    sznur_lista* pierwszy = new sznur_lista;
    int ile_sznurow;
    void konstruktor(){
        pierwszy->nastepny = NULL;
        ile_sznurow = 0;
    }

    void add_sznur(char id_1, char id_2, char id_3){ //dodawanie pustego sznura
        sznur* dodawany = new sznur;
        dodawany->konstrukcja(id_1, id_2, id_3);
        if(ile_sznurow == 0){
            pierwszy->obecny = dodawany;
            ile_sznurow++;
        }
        else{
            sznur_lista* newElem = new sznur_lista;
            newElem->obecny = dodawany;
            newElem->nastepny = pierwszy;
            pierwszy->poprzedni = newElem;
            pierwszy = newElem;
            pierwszy->poprzedni = NULL;
            ile_sznurow++;
        }
    }
    void display(){ //tymczasowe
        sznur_lista* najmniejszy = pierwszy;
        char a, b, c;
        a = b = c = '1';
        for(int j = 0; j < ile_sznurow; j++){
            int i = 0;
            for(sznur_lista* walkElem = pierwszy; i < ile_sznurow; walkElem = walkElem->nastepny, i++){

                if(pierwszy_mniejszy(a, b, c, walkElem->obecny->id_1, walkElem->obecny->id_2, walkElem->obecny->id_3)){
                    if(pierwszy_mniejszy(walkElem->obecny->id_1, walkElem->obecny->id_2, walkElem->obecny->id_3,
                        najmniejszy->obecny->id_1, najmniejszy->obecny->id_2, najmniejszy->obecny->id_3) ||
                       pierwszy_mniejszy(najmniejszy->obecny->id_1, najmniejszy->obecny->id_2, najmniejszy->obecny->id_3, a, b, c) ||
                       (najmniejszy->obecny->id_1 == a && najmniejszy->obecny->id_2 == b && najmniejszy->obecny->id_3 == c)){
                            najmniejszy = walkElem;
                    }
                }
            }
            najmniejszy->obecny->display();
            a = najmniejszy->obecny->id_1;
            b = najmniejszy->obecny->id_2;
            c = najmniejszy->obecny->id_3;
        }
    }
    sznur* find_sznur(char a, char b, char c){ //wyszukiwanie sznura o podanym identyfikatorze
        int i = 0;
        for(sznur_lista* walkElem = pierwszy; i < ile_sznurow ; walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_1 == a && walkElem->obecny->id_2 == b && walkElem->obecny->id_3 == c){
                return walkElem->obecny;
            }
        }
        return NULL;
    }
    void delete_sznur(char a, char b, char c){
        int i = 0;
         for(sznur_lista* walkElem = pierwszy; i < ile_sznurow ; walkElem = walkElem->nastepny, i++){
            if(walkElem->obecny->id_1 == a && walkElem->obecny->id_2 == b && walkElem->obecny->id_3 == c){ //sznur sie znalazl
                int j = 0;
                for(lista_sznura* tempElem = walkElem->obecny->pierwszy; j < walkElem->obecny->ile_korali; tempElem = tempElem->nastepny, j++){
                    tempElem->obecny->delete_everything();
                }
                /*lista_sznura* delElem = walkElem->obecny->pierwszy;
                while(j < walkElem->obecny->ile_korali){
                    lista_sznura* saveElem = delElem->nastepny;
                    delElem->obecny->delete_everything();
                    delElem = saveElem;
                    j++;
                }*/

                if(ile_sznurow == 1) ile_sznurow = 0;
                else{
                    if(i != 0){
                        walkElem->poprzedni->nastepny = walkElem->nastepny;
                    }
                    else{
                        walkElem->nastepny->poprzedni = NULL;
                        pierwszy = walkElem->nastepny;
                    }
                    if(i != ile_sznurow - 1){
                        walkElem->nastepny->poprzedni = walkElem->poprzedni;
                    }
                    else{
                        walkElem->poprzedni->nastepny = NULL;
                    }
                }
                ile_sznurow--;
            }
        }
    }
};


int main()
{

    konstrukcja* test = new konstrukcja;
    test->konstruktor();

    char wybor;

    do{
        cin >> wybor;

        if(wybor == 'S'){ //dodanie do konstrukcji pustego sznura
            char a, b, c;
            cin >> a >> b >> c;
            test->add_sznur(a, b, c);
        }

        else if(wybor == 'B'){ //dodanie do sznura abc korala x
            int x;
            char a, b, c;
            cin >> x;
            cin >> a >> b >> c;
            sznur* temp = new sznur;
            temp = test->find_sznur(a, b, c);
            if(temp != NULL) temp->add_koral(x);
        }

        else if(wybor == 'L'){ //wiazanie od korala x ze sznura abc do korala y ze sznura def
            int x;
            char a, b, c;
            int y;
            char d, e, f;
            cin >> x;
            cin >> a >> b >> c;
            cin >> y;
            cin >> d >> e >> f;
            sznur* temp = new sznur;
            temp = test->find_sznur(a, b, c);
            sznur* temp1 = new sznur;
            temp1 = test->find_sznur(d, e, f);
            if(temp != NULL && temp1 != NULL){
                koralik* tmp = new koralik;
                koralik* tmp1 = new koralik;
                tmp = temp->find_koral(x);
                tmp1 = temp1->find_koral(y);
                if(tmp != NULL && tmp1 != NULL){
                    tmp->powiazanie(tmp1);
                    tmp1->powiazanie_do(tmp);
                }
            }
        }

        else if(wybor == 'U'){ //usuniecie wiazania od korala x ze sznura abc do korala y ze sznura def
            int x;
            char a, b, c;
            int y;
            char d, e, f;
            cin >> x;
            cin >> a >> b >> c;
            cin >> y;
            cin >> d >> e >> f;
            sznur* temp = new sznur;
            temp = test->find_sznur(a, b, c);
            sznur* temp1 = new sznur;
            temp1 = test->find_sznur(d, e, f);
            if(temp != NULL && temp1 != NULL){
                koralik* tmp = new koralik;
                koralik* tmp1 = new koralik;
                tmp = temp->find_koral(x);
                tmp1 = temp1->find_koral(y);
                if(tmp != NULL && tmp1 != NULL){
                    tmp1->delete_powiazanie(tmp);
                }
            }
        }

        else if(wybor == 'D'){ //usuniecie korala x ze sznura abc wraz ze wszytkimi wiazaniami do kasowanego korala
            int x;
            char a, b, c;
            cin >> x;
            cin >> a >> b >> c;
            sznur* temp = test->find_sznur(a, b, c);
            if(temp != NULL) temp->delete_koral(x);
        }

        else if(wybor == 'M'){ //przenoszenie korala x ze sznura abc do sznura def wraz z wiazaniami
            int x;
            char a, b, c;
            char d, e, f;
            cin >> x;
            cin >> a >> b >> c;
            cin >> d >> e >> f;
            sznur* temp = test->find_sznur(a, b, c);
            sznur* temp1 = test->find_sznur(d, e, f);
            if(temp != NULL && temp1 != NULL){
                if(temp->ile_korali != 0){
                    temp->przenies_koral(x, temp1);
                }
            }
        }

        else if(wybor == 'R'){ //usuwanie sznura wraz ze wszystkimi koralami i wiazaniami do tych korali
            char a, b, c;
            cin >> a >> b >> c;
            test->delete_sznur(a, b, c);
        }

        else if(wybor == 'P'){ //wypisywanie wszyskiego w odpowiedniej kolejnosci
            test->display();
        }
    }
    while(wybor != 'F');

    delete test;

    return 0;
}
